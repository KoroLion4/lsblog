<?php    
    class Model
    {
        /**
         * Returns table name for model
         * 
         * @return string name of the table
         */
        private function get_table_name() 
        {
            return strtolower(get_class($this)).'s';
        }
        
        /**
         * Returns an array of class's properties
         *
         * @return array class's properties
         */
        private function get_properties()
        {
            $reflection = new ReflectionObject($this);
            $properties = $reflection->getProperties(ReflectionProperty::IS_PUBLIC);
            
            $props = array();
            foreach ($properties as $property) {
                $key = $property->getName();
                $value = $property->getValue($this);
                $props[$key] = $value;
            }
            
            return $props;
        }
        
        /**
         * Fill object's properties from an assoc array
         *
         * @param array $a assoc array of properties
         */
        private function set_properties($a) {
            $obj = new ReflectionObject($this);
            foreach ($a as $key => $value) {
                $property = $obj->getProperty($key);
                if (!$property->isPrivate()) {
                    $property->setValue($this, $value);
                }
            }
        }
        
        /**
         * Fill properties of this object from database
         *
         * @param $where string statement for PDO SQL WHERE (example: `id` = ?)
         * @param $values array elements of this array will replace ? of $where
         */
        public function get($where, $values)
        {
            $stmt = $GLOBALS['pdo']->prepare("SELECT * FROM `{$this->get_table_name()}` WHERE $where LIMIT 1;");
            $stmt->execute($values);
            
            if ($stmt->rowCount() == 1) {
                $object = $stmt->fetch(PDO::FETCH_ASSOC);
                $this->set_properties($object);
            }
        }
        
        /**
         * Get new values of this object from DB. Important! Object must has an id!
         */
        public function refresh_from_db()
        {
            if ($this->id) {
                $this->get('`id` = ?', array($this->id));
            }
        }
        
        function objects($where=null, $values=null)
        {
            if ($where)
                $where = 'WHERE '.$where;
            $stmt = $GLOBALS['pdo']->prepare("SELECT * FROM `{$this->get_table_name()}` $where;");
            $stmt->execute($values);
            $objects = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $res_objects = array();
            foreach ($objects as $object) {
                $res_obj = new User();
                $res_obj->set_properties($object);
                array_push($res_objects, $res_obj);
            }
            return $res_objects;
        }
        
        public function create()
        {
            $properties = $this->get_properties();
            
            $to_insert_columns = '';
            $to_insert_values = '';
            $values = array();
            foreach ($properties as $key => $value) {
                if ($key != 'id' && $value) {
                    $to_insert_columns .= "`$key`, ";
                    $to_insert_values .= '?, ';
                    array_push($values, $value);
                }
            }
            $to_insert_columns = substr($to_insert_columns, 0, strlen($to_insert_columns) - 2);
            $to_insert_values = substr($to_insert_values, 0, strlen($to_insert_values) - 2);
            
            $stmt = $GLOBALS['pdo']->prepare("INSERT INTO `{$this->get_table_name()}` ($to_insert_columns) VALUES ($to_insert_values);");
            $stmt->execute($values);
            
            $id = $GLOBALS['pdo']->lastInsertId();
            if ($id) {
                $this->id = $id;
                return true;
            } else {
                return false;
            }
        }
        
        public function save()
        {
            $properties = $this->get_properties();
            
            $values = array();
            $to_update = '';
            foreach ($properties as $key => $value) {
                if ($key != 'id') {
                    $to_update .= "`$key` = ?, ";
                    array_push($values, $value);
                }
            }
            array_push($values, $this->id);
            $to_update = substr($to_update, 0, strlen($to_update) - 2);
            
            $stmt = $GLOBALS['pdo']->prepare("UPDATE `{$this->get_table_name()}` SET $to_update WHERE `id` = ?;");
            $stmt->execute($values);
        }
    }
    
    class User extends Model
    {
        public $id;
        public $username;
        public $registered_datetime;
        
        public $pass_hash;
        public $session_key;
        
        function __construct($username=null) 
        {
            $this->username = $username;
        }
        
        private function generate_pass_hash($username, $password) 
        {
            $username = strtolower($username);
            $pass_hash = hash('sha256', $GLOBALS['settings']['core']['hash_salt'].$password.$username);
            for ($i = 0; $i < 100; $i++)
                $pass_hash = hash('sha256', $i.$pass_hash.$GLOBALS['settings']['core']['hash_salt'].$username);
            
            return $pass_hash;
        }
        
        public function is_authenticated() 
        {
            return ($this->id && $this->username)? true: false;
        }
        
        public function authenticate($username, $password) 
        {
            $pass_hash = $this->generate_pass_hash($username, $password);
            $this->get('`username` = ? AND `pass_hash` = ?', array($username, $pass_hash));
            return $this->is_authenticated();
        }
        
        public function logout($on_all_devices=false)
        {
            unset($_COOKIE['session_key']);
            setcookie('session_key', NULL, time() - 3600, '/', NULL, NULL, true);
            
            if ($on_all_devices) {
                $this->session_key = NULL;
                $this->save();
            }
        }
        
        public function login($remember=true)
        {
            $this->refresh_from_db();
            
            if (!$this->session_key) {
                $session_key = hash('sha256', time().$GLOBALS['settings']['core']['secret']);
                for ($i = 0; $i < 5; $i++)
                    $session_key = hash('sha256', $session_key.$i);
                $this->session_key = $session_key;
            }
            
            $this->save();
            $expires = ($remember)? time() + 30 * (24 * 3600): 0; // 30 days
            setcookie('session_key', $this->session_key, $expires, '/', NULL, NULL, true);
            
            $this->session_key = NULL;
            $this->pass_hash = NULL;
        }
        
        public function set_password($new_password) 
        {
            if ($this->username) {
                $this->pass_hash = $this->generate_pass_hash($this->username, $new_password);
            }
        }
    }
?>