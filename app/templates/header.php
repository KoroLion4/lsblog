<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/">LSBlog</a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?php if ($url == '/' || !$url) echo 'active'; ?>">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <?php if ($GLOBALS['user']->is_authenticated()): ?>
                    <li class="nav-item <?php if ($url == '/users/') echo 'active'; ?>">
                        <a class="nav-link" href="/users/">Список пользователей</a>
                    </li>
                <?php endif; ?>
            </ul>       
            <form method="POST" action="/logout/" class="form-inline mt-2 mt-md-0">
                <input type="hidden" name="csrf_token" value="<?php echo $GLOBALS['csrf_token']; ?>">
                <div class="dropdown mr-5">
                    <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <strong><?php echo $GLOBALS['user']->username; ?></strong>
                    </button>
                    <div class="dropdown-menu mt-1" aria-labelledby="dropdownMenu">
                        <a class="dropdown-item" href="/profile/<?php echo $GLOBALS['user']->username; ?>/">Профиль</a>
                        <div class="dropdown-divider"></div>
                        <button class="dropdown-item" name="sign_out">Выход</button>
                        <button class="dropdown-item" name="full_sign_out" style="color: red;">Полный выход</button>
                    </div>
                </div>
            </form>
        </div>
    </nav>
</header>