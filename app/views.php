<?php    
    function app_view($url) {
        require('templates/app.php');
    }
    
    function users_list_view($url) {
        $users = (new User())->objects();
        require('templates/users_list.php');
    }
    
    function profile_view($url) {
        $username = explode('/', $url)[2];
        $user = new User();
        $user->get('`username` = ?', array($username));
        if ($user->id) {
            $owner = $user->username === $GLOBALS['user']->username;
            require('templates/profile.php');
        } else {
            return http_response_not_found();
        }
    }
    
    function registration_view($url) {
        if ($GLOBALS['user']->is_authenticated()) {
            return redirect('/');
        }
        
        $error = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $recaptcha_enabled = (bool)$GLOBALS['settings']['recaptcha']['enabled'];
            if (!$recaptcha_enabled || check_recaptcha($_POST, $GLOBALS['settings']['recaptcha']['private_key'])) {
                $username = $_POST['username'];
                if (preg_match('/^[a-zA-Z0-9\d_\d-]{2,30}$/i', $username)) {
                    if ($_POST['password'] === $_POST['confirm_password']) {
                        $new_user = new User($username);
                        $new_user->registered_datetime = date('Y-m-d H:i:s');
                        $new_user->set_password($_POST['password']);
                        if ($new_user->create()) {
                            return redirect('/auth/?message=Успешная регистрация!');
                        } else {
                            $error = 'К сожалению, данный логин занят =(';
                        }
                    }
                }
            } else {
                $error = 'Вы что, робот!?';
            }
            
            if (!$error)
                $error = 'Ошибка регистрации!';
        }
        require('templates/registration.php');
    }
    
    function auth_view($url) {
        if ($GLOBALS['user']->is_authenticated()) {
            return redirect('/');
        }

        $error = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $recaptcha_enabled = (bool)$GLOBALS['settings']['recaptcha']['enabled'];
            if (!$recaptcha_enabled || check_recaptcha($_POST, $GLOBALS['settings']['recaptcha']['private_key'])) {
                $username = $_POST['username'];
                if (preg_match('/^[a-zA-Z0-9\d_\d-]{2,30}$/i', $username)) {
                    $GLOBALS['user']->authenticate($username, $_POST['password']);
                    if ($GLOBALS['user']->is_authenticated()) {
                        $GLOBALS['user']->login(isset($_POST['remember']));
                        
                        return redirect('/');
                    }
                }
            } else {
                $error = 'Вы что, робот!?';
            }
            if (!$error)
                $error = 'Неверный логин или пароль!';
        }
        require('templates/auth.php');
    }
    
    function logout_view($url) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['full_sign_out']))
                $GLOBALS['user']->logout(true);
            else
                $GLOBALS['user']->logout();
            
            return redirect('/');
        } else {
            return http_response_method_not_allowed();
        }
    }
?>