<!DOCTYPE html>
<html>
<head>
    <?php include './static/to_head.html'; ?>
    
    <title>Список пользователей</title>
</head>
<body>
    <?php include 'header.php'; ?>
    
    <h2 class="text-center mt-4">Список пользователей</h2>
    <div class="users-list w-75 mx-auto mt-4">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Логин</th>
                    <th scope="col">Дата регистрации</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($users): ?>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <th scope="row"><?php echo $user->id; ?></th>
                            <td><a href="/profile/<?php echo $user->username; ?>/"><?php echo $user->username; ?></a></td>
                            <td><?php echo convert_datetime($user->registered_datetime)->format('d-m-Y H:i'); ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?php include './static/to_body_end.html'; ?>
</body>
</html>