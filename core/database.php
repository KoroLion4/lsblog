<?php 
    # https://stackoverflow.com/questions/134099/are-pdo-prepared-statements-sufficient-to-prevent-sql-injection
        
    try {
        $GLOBALS['pdo'] = new PDO(
            $GLOBALS['settings']['database']['driver'].':host='.$GLOBALS['settings']['database']['host'].';dbname='.$GLOBALS['settings']['database']['dbname'],
            $GLOBALS['settings']['database']['username'],
            $GLOBALS['settings']['database']['password']
        );
        $GLOBALS['pdo']->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    } catch (PDOException $e) {
        throw new Exception('ERROR: Unable to connect to the database!');
    }
?>