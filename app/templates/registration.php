<!DOCTYPE html>
<html>
<head>
    <?php include './static/to_head.html'; ?>
     
    <title>Регистрация</title>
</head>
<body>
    <div class="registration w-50 mx-auto my-5">
        <form method="POST">
            <input type="hidden" name="csrf_token" value="<?php echo $GLOBALS['csrf_token']; ?>">
            
            <h1 class="text-center">Регистрация</h1>
            <div class="form-group text-center lead">
                Уже есть аккаунт? <a href="/auth/"><strong>Вход</strong></a>
            </div>
            <?php if ($error): ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
            <?php endif; ?>
            <div class="form-group">
                <label for="inputUsernameReg">Логин</label>
                <input type="text" class="form-control" id="inputUsernameReg" name="username" placeholder="Введите логин">
                <small class="form-text text-muted">От 2-х до 30-ти символов. Строчные и заглавные латинские буквы, цифры, а также - и _</small>
            </div>
            <div class="form-group">
                <label for="inputPasswordReg">Пароль</label>
                <input type="password" class="form-control" id="inputPasswordReg" name="password" placeholder="Введите пароль">
            </div>
            <div class="form-group">
                <label for="inputConfirmPasswordReg">Подтверждение пароля</label>
                <input type="password" class="form-control" id="inputConfirmPasswordReg" name="confirm_password" placeholder="Подтвердите пароль">
            </div>
            <?php if ((bool)$GLOBALS['settings']['recaptcha']['enabled']): ?>
                <div class="form-group">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha mx-auto" style="width: 304px;" data-sitekey="<?php echo $GLOBALS['settings']['recaptcha']['public_key'] ?>"></div>
                </div>
            <?php endif; ?>
            <div class="form-group text-center mt-3">
                <input type="submit" class="btn btn-lg btn-primary w-50" name="register" value="Создать аккаунт">
            </div>
        </form>
    </div>
    
    <?php include './static/to_body_end.html'; ?>
</body>
</html>