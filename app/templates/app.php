<!DOCTYPE html>
<html>
<head>
    <?php include './static/to_head.html'; ?>
    
    <title>LSBlog</title>
</head>
<body>
    <?php include 'header.php'; ?>
    
    <div class="cover-image w-100 p-3 text-center">
        <div class="cover">
            <div class="content">
                <h1 class="cover-heading">Hello, Codenrock.com, I`m <?php echo $GLOBALS['user']->username; ?></h1>
                <p class="lead mt-3"><a href="https://codenrock.com/" target="_blank"><strong>Codenrock</strong></a> - международная площадка с онлайн конкурсами для IT - специалистов. Найди работу мечты или выиграй денежные призы.</p>
                <p class="lead mt-4">
                    <a href="https://codenrock.com/" target="_blank" class="btn btn-lg btn-primary">Узнать больше</a>
                </p>
            </div>
        </div>
    </div>
    
    <h1 class="mt-3 text-center">Light Simple Blog</h1>
    <div class="container text-center mt-5 pb-5">
        <div class="row">
            <div class="col-lg-4">
                <img src="/static/images/icons/speedmeter.png" alt="">
                <h2 class="mt-2">Скорость</h2>
                <p>Используется собственный максимально облегчённый фреймворк только с самым важным функционалом.</p>
            </div>
            <div class="col-lg-4">
                <img src="/static/images/icons/lock.png" alt="">
                <h2 class="mt-2">Безопасноть</h2>
                <p>Защита от CSRF и SQL инъекций. Вопросам безопасности уделяется особое внимание.</p>
            </div>
            <div class="col-lg-4">
                <img src="/static/images/icons/suitcase.png" alt="Generic placeholder image">
                <h2 class="mt-2">Гибкость</h2>
                <p>Собственная реализация схемы MVC (model-view-controller), позволяет быстро и оперативно изменять и дорабатывать проект.</p>
            </div>
        </div>
    </div>
    
    <footer class="text-center">
        <p>Task for <a href="https://codenrock.com/">codenrock.com</a>, by <a href="https://github.com/KoroLion/">KoroLion</a>. 2018.</p>
    </footer>
    
    <?php include './static/to_body_end.html'; ?>
</body>
</html>