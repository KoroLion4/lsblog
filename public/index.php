<?php
    require('../core/core.php');
    
    $csrf_ok = true;
    // middlewares
    require('../core/middlewares/csrf_protection.php');
    require('../core/middlewares/user_authentication_by_cookie.php');
    
    if ($csrf_ok) {
        require('../app/urls.php');
        
        $found = false;
        $current_url = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
        foreach ($urls as $url => $view) {
            if (preg_match(url($url), $current_url)) {
                $view($current_url);
                $found = true;
                break;
            }
        }
        if (!$found) {
            http_response_not_found();
        }
    } else {
        http_response_forbidden();
    }
    
    // should it be there? 
    // http://php.net/manual/en/pdo.connections.php says connection will be closed automatically...
    $GLOBALS['pdo'] = null;
?>