<?php    
    function redirect($url) {
        header("Location: ".$url);
    }
    
    function http_response_forbidden() {
        http_response_code(403);
        echo '<h1>ERROR 403: Forbidden!</h1>';
    }
    
    function http_response_not_found() {
        http_response_code(404);
        echo '<h1>ERROR 404: Page not found!</h1>';
    }
    
    function http_response_method_not_allowed() {
        http_response_code(405);
        echo '<h1>ERROR 405: Method not allowed!</h1>';
    }
    
    function url($url) {
        $url = str_replace('/', '\/', $url);
        $url = str_replace('<string>', '([a-zA-Z0-9\d_\d-])+', $url);
        $url = str_replace('<int>', '([0-9])+', $url);
        return "/^$url$/";
    }
    
    /**
     * Converts UTC datetime string to specified timezone
     *
     * @param $datetime_str string UTC datetime string, if null - returns current datetime in specified timezone
     * @param $timezone string Datetime will be converted to this, if not specified - timezone from settings will be used.
     *
     * @return object PHP datetime object
     */
    function convert_datetime($datetime_str=null, $timezone=null) {
        if (!isset($timezone))
            $timezone = $GLOBALS['settings']['core']['default_timezone'];
        $date = new DateTime($datetime_str, new DateTimeZone('UTC'));
        $date->setTimezone(new DateTimeZone($timezone));
        return $date;
    }
    
    function check_recaptcha($post, $key) {
        if (isset($post['g-recaptcha-response'])) {
            $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                    'secret' => $key,
                    'response' => $post['g-recaptcha-response']
                ))
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if ($GLOBALS['debug'] === true) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            }
            $response = curl_exec($ch);
            curl_close($ch);
            if ($response === false) {
                return false;
            } else {
                $response = json_decode($response);
                if ($response->success === true)
                    return true;
                else
                    return false;
            }
        } else {
            return false;
        }
    }
?>