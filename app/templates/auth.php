<!DOCTYPE html>
<html>
<head>
    <?php include './static/to_head.html'; ?>
    
    <title>Вход</title>
</head>
<body>
    <div class="auth mx-auto my-5 mx-1">
        <form method="POST">
            <input type="hidden" name="csrf_token" value="<?php echo $GLOBALS['csrf_token']; ?>">
        
            <h1 class="text-center">Вход</h1>
            <div class="form-group text-center lead">
                Нету аккаунта? <a href="/registration/"><strong>Регистрация</strong></a>
            </div>
            <?php if (isset($_GET['message'])): ?>
                <div class="alert alert-success"><?php echo $_GET['message']; ?></div>
            <?php endif; ?>
            <?php if ($error): ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
            <?php endif; ?>
            <div class="form-group">
                <label for="inputUsernameAuth">Логин</label>
                <input type="text" class="form-control" id="inputUsernameAuth" name="username" placeholder="Введите логин">
            </div>
            <div class="form-group">
                <label for="inputPasswordAuth">Пароль</label>
                <input type="password" class="form-control" id="inputPasswordAuth" name="password" placeholder="Введите пароль">
            </div>
            <?php if ((bool)$GLOBALS['settings']['recaptcha']['enabled']): ?>
                <div class="form-group">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha mx-auto" style="width: 304px;" data-sitekey="<?php echo $GLOBALS['settings']['recaptcha']['public_key'] ?>"></div>
                </div>
            <?php endif; ?>
            <div class="form-group text-center">
                <input type="checkbox" name="remember" id="rememberAuth" class="mr-1">
                <label for="rememberAuth" class="lead">Запомнить меня</label>
            </form>
            <div class="form-group text-center mt-2">
                <input type="submit" class="btn btn-lg btn-primary w-50" name="sign_in" value="Войти">
            </div>
        </form>
    </div>

    <?php include './static/to_body_end.html'; ?>
</body>
</html>