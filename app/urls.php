<?php    
    require('views.php');
    require('../core/decorators.php');

    $urls = array(
        '/' => auth_required('app_view'),
        '/users/' => auth_required('users_list_view'),
        '/profile/<string>/' => auth_required('profile_view'),
        '/registration/' => 'registration_view',
        '/auth/' => 'auth_view',
        '/logout/' => 'logout_view'
    );
?>