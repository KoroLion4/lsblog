<?php    
    $GLOBALS['settings'] = parse_ini_file('../config.ini', true);
    if (!$GLOBALS['settings'])
        throw new Exception('ERROR: Unable to read config.ini!');
    
    if (empty($GLOBALS['settings']['core']['secret']) || empty($GLOBALS['settings']['core']['hash_salt'])) {
        throw new Exception('ERROR: Secret or hash_salt are not set in config.ini!');
    }
    $GLOBALS['debug'] = (bool)$GLOBALS['settings']['core']['debug'];
    
    if ($GLOBALS['debug'])
        error_reporting(E_ALL);
    else {
        error_reporting(0);
    }
    
    date_default_timezone_set("UTC");
    
    require('database.php');
    require('models.php');
    
    $GLOBALS['user'] = new User();
    $GLOBALS['csrf_token'] = null;
    
    require('helper_functions.php');
?>