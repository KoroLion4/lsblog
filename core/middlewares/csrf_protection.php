<?php
    if (!isset($_COOKIE['csrf_token'])) {
        $csrf_token = hash('sha256', $GLOBALS['settings']['core']['secret'].time());
        for ($i = 0; $i < 3; $i++)
            $csrf_token = hash('sha256', $i.$csrf_token);
        setcookie('csrf_token', $csrf_token, 0, '/', NULL, NULL, true);
        $GLOBALS['csrf_token'] = $csrf_token;
    } else {
        $GLOBALS['csrf_token'] = $_COOKIE['csrf_token'];
    }
   
    if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
        if (!isset($_REQUEST['csrf_token']) || $_REQUEST['csrf_token'] != $_COOKIE['csrf_token']) {
            $csrf_ok = false;
        }
    }
?>