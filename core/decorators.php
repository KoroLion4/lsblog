<?php    
    function auth_required($view) {
        if (!$GLOBALS['user']->is_authenticated()) {
            return function () {
                redirect('/auth/');
            };
        } else {
            return $view;
        }
    }
?>