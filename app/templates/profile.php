<!DOCTYPE html>
<html>
<head>
    <?php include './static/to_head.html'; ?>
    
    <title><?php echo $user->username; ?></title>
</head>
<body>
    <?php include 'header.php'; ?>
    
    <div class="profile container text-center">
        <h1 class=""><?php echo $user->username; ?></h1>
        <div class="row lead">
            <div class="col-4"><strong>Логин:</strong></div>
            <div class="col-4"><?php echo $user->username; ?></div>
        </div>
        <div class="row lead">
            <div class="col-4"><strong>Дата регистрации:</strong></div>
            <div class="col-4"><?php echo convert_datetime($user->registered_datetime)->format('d-m-Y H:i'); ?></div>
        </div>
    </div>
    
    <?php include './static/to_body_end.html'; ?>
</body>
</html>